# Copyright (c) 2019 jrolsmith under MIT License.

import socket
import struct

UDP_IP = "127.0.0.1"
#UDP_IP = "192.168.1.89"
UDP_PORT = 2808

dash_format = '<iIfffffffffffffffffffffffffffffffffffffffffffffffffffiiiiifffffffffffffffffHBBBBBBbbb'

charRevCtrMax = 180
charRevCtrMin = 0
charRevCtrTrue = '#'
charRevCtrFalse = '_'

def map(x, in_min, in_max, out_min, out_max):
    return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;

def drawConsoleBar(revTrueChar, revFalseChar, numTrueChars, maxChars):
    print "[%s%s]" % ((revTrueChar * numTrueChars), (revFalseChar * (maxChars - numTrueChars)))
    
def main():
    print ('UDP Logger')
    
    sock = socket.socket(socket.AF_INET, # Internet
    socket.SOCK_DGRAM) # UDP
    sock.bind((UDP_IP, UDP_PORT))   

    while True:
      data, addr = sock.recvfrom(1024) # buffer size is 1024 bytes
      dataStruct = struct.unpack(dash_format,data);
      
      if dataStruct[0]: # If racing...
          # Draw rev counter bar in console
          numChars = int(map(dataStruct[4], dataStruct[3], dataStruct[2], charRevCtrMin, charRevCtrMax))
          drawConsoleBar(charRevCtrTrue, charRevCtrFalse, numChars, charRevCtrMax)
 
    sock.close()
    print('Goodbye')
    
if __name__ == '__main__':
    main()
