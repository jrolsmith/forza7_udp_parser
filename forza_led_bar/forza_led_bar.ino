// Copyright (c) 2019 jrolsmith under MIT License.

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN 6
#define NUM_LEDS 5
#define LIMIT 255
#define STEP (LIMIT / NUM_LEDS)

uint8_t RED[] = {255,0,0};
uint8_t GREEN[] = {0,255,0};
uint8_t ORANGE[] = {255,128,0};
uint8_t YELLOW[] = {255,255,0};
uint8_t BLACK[] = {0,0,0};
uint8_t *ledColourMap[NUM_LEDS] = {GREEN, GREEN, YELLOW, ORANGE, RED};

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUM_LEDS, PIN, NEO_GRB + NEO_KHZ800);

// IMPORTANT: To reduce NeoPixel burnout risk, add 1000 uF capacitor across
// pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input
// and minimize distance between Arduino and first pixel.  Avoid connecting
// on a live circuit...if you must, connect GND first.

void setup() {
	// This is for Trinket 5V 16MHz, you can remove these three lines if you are not using a Trinket
	#if defined (__AVR_ATtiny85__)
	if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
	#endif
	// End of trinket special code
	strip.begin();
	strip.show(); // Initialize all pixels to 'off'

	Serial.begin(115200);
	while (!Serial) {}// wait for serial port to connect. Needed for native USB port only 
}

void loop() {
  if (Serial.available() > 0) {
    // get incoming byte:
    int numLEDs = Serial.read();
	
	if(numLEDs <=NUM_LEDS){
		uint8_t *ledColour;
		
		for(uint8_t led = 0; led < NUM_LEDS; led++) {
		  if( led > 0 && led <= numLEDs ) {
			ledColour = ledColourMap[led];
		  } else {
			ledColour = BLACK;        
		  }
		  
		  strip.setPixelColor(led, ledColour[0], ledColour[1], ledColour[2]);	// R, G, B
		}
		strip.show();
	}
  }
}
